 #!/bin/bash

 file_path=$1
 songs=$2
 for f in find "$file_path" -name *"$songs"*
 do
      nohup ffplay -autoexit "$f"
 done
