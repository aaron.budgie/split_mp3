 #!/bin/bash

 file_path=$1
 for f in $file_path/*
 do
     nohup ffmpeg -i "$f" "$f.mp3"
 done
