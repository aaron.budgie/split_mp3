# first download and get vol.txt:
# youtube-dl -F <url>
# youtube-dl -f 140 <url>
# ffmpeg -i ji.m4a ji.mp3
# ffmpeg -i ji.mp3 -af silencedetect=noise=-12dB:d=1.5 -f null - 2> vol.txt
import re
import subprocess
import argparse

parser = argparse.ArgumentParser(
    description='''Split long mp3 file by silence''')
parser.add_argument('-f', '--file', help="input file")
parser.add_argument('-db', '--decibel', help="threshold", default=-12)
parser.add_argument('-d', '--dur', help="silence dur", default=1.5)
parser.add_argument('-t', '--test', help="it is a test", default="no")
parser.add_argument(
    '-v', '--vol', help="silence already computed", default="no")

args = vars(parser.parse_args())


# Define our connection string


def main():
    file = args['file']
    db = args['decibel']
    d = args['dur']
    test = args['test']
    vol_exist = args['vol']
    print args
    if vol_exist == 'no':
        command = "ffmpeg -i %s -af silencedetect=noise=%sdB:d=%s -f null - 2> vol.txt" % (file, db, d)
        subprocess.call(command, shell=True)
    with open('vol.txt') as f:
        content = f.readlines()
    res = []
    start = 0.0
    end = 0.0
    prev_start = 0.0
    for line in content:
        pos = [(m.start(0), m.end(0)) for m in re.finditer(r'\bsilence_start\b', line)]
        if pos:
            p_end = pos[0][0]
            end = line[p_end+15:p_end+20]
            end = float(end)
            if not start:
                res.append((0.0, end))
        pos = [(m.start(0), m.end(0)) for m in re.finditer(r'\bsilence_end\b', line)]
        if pos:
            p_start = pos[0][0]
            start = line[p_start+13:p_start+18]
            start = float(start)
            if not prev_start:
                prev_start = start
        if start != prev_start:
            dur = end - prev_start
            if dur > 120:
                res.append((prev_start + 2, dur + 2))
                prev_start = end
    i = 0

    with open('split.txt', 'w') as f:
        for item in res:
            f.write("%s,%s\n" % item)
            i += 1
            command = "ffmpeg -ss %s -t %s -i %s %s-%s.mp3" % (str(item[0]), str(item[1]), file, file, str(i))
            print(command)
            if test == 'no':
                subprocess.call(command, shell=True)
    print(res)


if __name__ == '__main__':
    main()
