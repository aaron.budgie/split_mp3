#!/bin/bash
file=$1
file2="2_${file}"
file3="3_${file}"
file4="FF_${file}"
echo $file
echo $file2
ffmpeg -i $file -af "highpass=f=300, lowpass=f=2500" $file2
ffmpeg -i $file2 -filter:a "volume=3.0" $file3
ffmpeg -i $file3 -filter_complex "[0:v]setpts=0.625*PTS[v];[0:a]atempo=1.6[a]" -map "[v]" -map "[a]" $file4
rm $file2
rm $file3


